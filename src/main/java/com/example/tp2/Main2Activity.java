package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent() ;
        final Book book = (Book) intent.getExtras().get("book") ;
        final long id = book.getId() ;

        TextView textViewName = (TextView) findViewById(R.id.nameBook) ;
        textViewName.setText(book.getTitle());

        TextView textViewAuthor = (TextView) findViewById(R.id.editAuthors) ;
        textViewAuthor.setText(book.getAuthors());

        TextView textViewYears = (TextView) findViewById(R.id.editYear) ;
        textViewYears.setText(book.getYear());

        TextView textViewGenres = (TextView) findViewById(R.id.editGenres) ;
        textViewGenres.setText(book.getGenres());

        TextView textViewPublisher = (TextView) findViewById(R.id.editPublisher) ;
        textViewPublisher.setText(book.getPublisher());


        Button sauvegarder = (Button) findViewById(R.id.sauvegarder) ;

        sauvegarder.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                //Récupère les données écrites dans la textView
                EditText editName = (EditText) findViewById(R.id.nameBook) ;
                EditText editAuthors = (EditText) findViewById(R.id.editAuthors) ;
                EditText editYear = (EditText) findViewById(R.id.editYear) ;
                EditText editGenre =(EditText) findViewById(R.id.editGenres) ;
                EditText editPublisher = (EditText) findViewById(R.id.editPublisher) ;

                //On caste les donées récupéré en leurs type d'origine
                String name = editName.getText().toString() ;
                String author = editAuthors.getText().toString() ;
                String year = editYear.getText().toString() ;
                String genre = editGenre.getText().toString() ;
                String publisher = editPublisher.getText().toString() ;

                //Si toutes les champs sont bien remplies
                if(!name.isEmpty() && !author.isEmpty() && !year.isEmpty() && !genre.isEmpty() && !publisher.isEmpty())
                {
                    Book book = new Book(id, name, author, year, genre, publisher) ;
                    BookDbHelper bookDbHelper = new BookDbHelper(Main2Activity.this) ;
                    bookDbHelper.updateBook(book) ;

                    Toast.makeText(Main2Activity.this, "Sauvegarde effectué",
                          Toast.LENGTH_LONG).show();


                    Intent intent1 = new Intent(Main2Activity.this,MainActivity.class) ;
                    startActivity(intent1);
                }
                //Si un champs n'est pas remplit
                else
                {
                    Toast.makeText(Main2Activity.this, "Un champs est vide",
                                   Toast.LENGTH_LONG).show();
                }
            } ;
        }) ;
    }
}
