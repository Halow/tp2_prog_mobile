package com.example.tp2;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import static android.R.layout.simple_list_item_1;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Afficher la liste des livres contenu dans la BDD
        BookDbHelper bookDbHelper = new BookDbHelper(this) ;

        /*
          --------------------------------------------------
          On le met uniquement au début quand on lance l'emulateur afin de pouvoir récupérer la base de données
          Ou alors quand on s'est trompé dans la suppression et qu'on veut retrouver la BDD d'origine
        */
        //bookDbHelper.delete();
        //bookDbHelper.populate();
        Cursor cursor = bookDbHelper.fetchAllBooks() ;


        String[] coloms = new String[]  {
                BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS
        } ;

        ListView listView = (ListView) findViewById(R.id.listView);

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                coloms,
                new int[] {android.R.id.text1, android.R.id.text2});

        listView.setAdapter(adapter);


        //Pour afficher les informations complémentaire
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                // Do something in response to the click
                Intent intent = new Intent(MainActivity.this, Main2Activity.class) ;
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                Book book = BookDbHelper.cursorToBook(cursor) ;
                intent.putExtra("book", book) ;
                startActivity(intent);
            }
        }) ;

        //Quand on appuie longtemps sur le click, permet de supprimer
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                final Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                alertDialog.setNeutralButton("Supprimer", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        BookDbHelper bookDbHelper1 = new BookDbHelper(MainActivity.this) ;
                        bookDbHelper1.deleteBook(cursor);

                        Intent intent = new Intent(MainActivity.this, MainActivity.class) ;
                        startActivity(intent);

                        Toast.makeText(MainActivity.this, "Suppression effectué",
                                Toast.LENGTH_LONG).show();
                    }
                });
                alertDialog.show() ;
                return true ;
            }
        });

        //Ajouter un livre
        FloatingActionButton ajouter = (FloatingActionButton) findViewById(R.id.ajouter) ;

        ajouter.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                Intent intent = new Intent(MainActivity.this, AddBook.class) ;
                startActivity(intent);
            } ;
        }) ;
    }
}
